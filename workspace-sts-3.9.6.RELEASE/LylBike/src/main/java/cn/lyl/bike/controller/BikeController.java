package cn.lyl.bike.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.lyl.bike.proj.Bike;
import cn.lyl.bike.service.BikeService;

/**
 * 用于标记这个类是用来接受请求和相应用户的一个控制器
 * 加上注解后，spring容器就会对他进行实例化
 * @author LYL
 *
 */
@Controller
public class BikeController {
	
	@Autowired
	private BikeService bikeService;
	
	@RequestMapping("/bike/add")
	@ResponseBody
	public String add(@RequestBody Bike bike) {
		//调用service,将数据保存在MongoDB中
		bikeService.save(bike);
		return "success";
	}

}
