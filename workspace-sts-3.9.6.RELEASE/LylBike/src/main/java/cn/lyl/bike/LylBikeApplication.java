package cn.lyl.bike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//为springboot的入口程序，在spring启动时，就会进行扫描带有特殊注解的类
@SpringBootApplication
public class LylBikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LylBikeApplication.class, args);
	}
}
