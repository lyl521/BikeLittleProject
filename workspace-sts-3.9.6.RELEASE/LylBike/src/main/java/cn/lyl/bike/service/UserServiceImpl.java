package cn.lyl.bike.service;

import java.util.concurrent.TimeUnit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.github.qcloudsms.SmsSingleSender;

import cn.lyl.bike.proj.User;

@Service
public class UserServiceImpl implements UserService {

	

	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public boolean sendMsg(String countryCode, String phoneNum) {

		//调用腾讯云的短信App
		int appid=Integer.parseInt(stringRedisTemplate.opsForValue().get("appid"));
		String appkey=stringRedisTemplate.opsForValue().get("appkey");
		boolean flag=true;
		
		//生成一个随机的数字（四位）
		String code=(int)((Math.random()*9+1)*1000)+"";
	    SmsSingleSender ssender = new SmsSingleSender(appid,appkey);
		try {

			//相对应手机号的用户发送短信
			ssender.send(0,countryCode,phoneNum,
				    "您的登录验证码为"+ code +"，如非本人操作，请忽略本短信","","");
			
			//将发送的手机号做为key,将验证码作为value，保存在redis中
			stringRedisTemplate.opsForValue().set(phoneNum,code,300,TimeUnit.SECONDS);
			
		} catch (Exception e) {
			
			flag=false;
			e.printStackTrace();
		} 
	
		return flag; 
	}

	@Override
	public boolean verify(String phoneNum, String verifyCode) {
		boolean flag=false; 
		// 从redis中获取验证码，进行校验
		String code=stringRedisTemplate.opsForValue().get(phoneNum);
		if(code!=null && code.equals(verifyCode)) {
			
			flag=true;
		}
		return flag;
	}

	@Override
	public void register(User user) {

		mongoTemplate.insert(user);

	}

	@SuppressWarnings("unused")
	@Override
	public void update(User user) {
		// 如果对应的_id不存在，就插入，存在，就更新
		//使用insert,是另外插入一条数据，无法进行已经插入的数据执行更新
		//mongoTemplate.insert(user);
		//所以，要使用updateFrist,主要因为目前只有一条数据
		Update update=new Update();
		if(user.getDeposit()!=null) {
			update.set("deposit", user.getDeposit());
		}
		if(user.getStatus()!=null) {
			update.set("status", user.getStatus());
		}
		if(user.getName()!=null) {
			update.set("name", user.getName());
		}
		if(user.getIdNum()!=null) {
			update.set("idNum",user.getIdNum());
		}
		mongoTemplate.updateFirst(new Query(Criteria.where("phoneNum").is(user.getPhoneNum())), 
				update, User.class);
		

	}

}
