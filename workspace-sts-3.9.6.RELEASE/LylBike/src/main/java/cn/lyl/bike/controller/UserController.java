package cn.lyl.bike.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.lyl.bike.proj.User;
import cn.lyl.bike.service.UserService;

@Controller
public class UserController {
	
	
	/**
	 * 依赖注入
	 */
	@Autowired
	private UserService userService;
	
	//获取验证码
	@RequestMapping("/user/genCode")
	@ResponseBody
	public boolean genVerifyCode(String countryCode,String phoneNum) {
		boolean flag= userService.sendMsg(countryCode,phoneNum); 
		return flag;
	}
	
	//核对验证码
	@RequestMapping("/user/verify")
	@ResponseBody
	public boolean verify(String phoneNum,String verifyCode) {
			
		return userService.verify(phoneNum,verifyCode);
	}
	
	//用户不存在，则进行注册
	@RequestMapping("/user/register")
	@ResponseBody
	public boolean reg(@RequestBody User user) {//可以在参数前面加一个@RequestBody,接受json类型的数据，然后set到对应的实体类中的属性
		boolean flag=true;
		try {
			userService.register(user);
		} catch (Exception e) {
			e.printStackTrace();
			//以后会引入log机制，出现异常将进行异常记录
			flag=false;
		}
		return flag;
	}

	@RequestMapping("/user/deposit")
	@ResponseBody
	public boolean deposit(@RequestBody User user) {
		
		boolean flag=true;
		
		try {
			userService.update(user);
		} catch (Exception e) {
			e.printStackTrace();
			flag=false;			
		}
		return flag;
	}
	
	@RequestMapping("/user/identify")
	@ResponseBody
	public boolean identify(@RequestBody User user) {
		
		boolean flag=true;
		
		try {
			userService.update(user);
		} catch (Exception e) {
			e.printStackTrace();
			flag=false;			
		}
		return flag;
	}
}
