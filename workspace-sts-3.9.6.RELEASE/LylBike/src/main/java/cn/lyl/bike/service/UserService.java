package cn.lyl.bike.service;

import cn.lyl.bike.proj.User;

public interface UserService {

	boolean sendMsg(String countryCode, String phoneNum);

	boolean verify(String phoneNum, String verifyCode);

	void register(User user);

	void update(User user);

}
