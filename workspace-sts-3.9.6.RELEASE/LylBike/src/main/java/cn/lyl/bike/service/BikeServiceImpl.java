package cn.lyl.bike.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import cn.lyl.bike.proj.Bike;

@Service
public class BikeServiceImpl implements BikeService {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public void save(Bike bike) {
		// 调用具体的业务
		//mongoTemplate.insert(bike,"bikes");
		mongoTemplate.insert(bike);
		
	}

}
