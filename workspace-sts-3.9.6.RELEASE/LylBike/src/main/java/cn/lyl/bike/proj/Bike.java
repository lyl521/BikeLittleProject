package cn.lyl.bike.proj;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

//同MongoDB中的bikes collection进行关联
@Document(collection="bikes")
public class Bike {
	
	//主键，建立索引（唯一），id 对应  MongDB中的_id
	@Id
	private String id;
	private double latitude;
	private double longitude;
	
	//车辆的唯一标识，建立索引，方便以后查询速率提高
	@Indexed
	private Long bikeNo;
	public Long getBikeNo() {
		return bikeNo;
	}
	public void setBikeNo(Long bikeNo) {
		this.bikeNo = bikeNo;
	}
	private int status;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	

}
