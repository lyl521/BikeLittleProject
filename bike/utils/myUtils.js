


function get(key){
  var value = wx.getStorageSync(key)
  //如果没有取到
  if (!value) {
    value = getApp().globalData.status;
  }
  return value;
}
//将方法打包
module.exports = {
  get
}