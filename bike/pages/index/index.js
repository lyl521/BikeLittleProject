//index.js
//获取应用实例
//导包
var myUtils=require("../../utils/myUtils.js")



Page({
  data: {
    latitude:0,
    longitude:0,
    controls:[],
    markers:[]
  },
//首次加载页面时调用
  onLoad: function () {

   //将当前page this页面赋值给that
   var that=this;

   wx.getLocation({
     success: function(res) {

       //获取当前设备的经纬度（在Sensor中进行模拟）
       var latitude=res.latitude;
       var longitude=res.longitude
      
      //通过that获取data中的经纬度变量，将经纬度赋值给data中变量
       that.setData({
         latitude:latitude,
         longitude:longitude,

       })
     },
   })

    //获取设备信息
    wx.getSystemInfo({
      success: function (res) {
        var windowWidth=res.windowWidth;
        var windowHeight=res.windowHeight;

        //获取控件
        that.setData({
          controls: [
            //扫码按钮
            {
              id: 1,
              iconPath: '/images/1.png',
              position: {
                width: 140,
                height: 50,
                left: windowWidth/2-70,
                top: windowHeight-80

              },
              //是否可点击
              clickable:true
            },

            //充值按钮
            {
              id: 2,
              iconPath: '/images/2.png',
              position: {
                width: 30,
                height: 30,
                left: windowWidth -50,
                top: windowHeight - 130

              },
              //是否可点击
              clickable: true
            },

            //报修按钮
            {
              id: 3,
              iconPath: '/images/3.png',
              position: {
                width: 30,
                height: 30,
                left: windowWidth - 50,
                top: windowHeight - 80

              },
              //是否可点击
              clickable: true
            },

            //返回原位置按钮
            {
              id: 4,
              iconPath: '/images/4.png',
              position: {
                width: 30,
                height: 30,
                left: 20,
                top: windowHeight - 80

              },
              //是否可点击
              clickable: true
            },

            //位置按钮
            {
              id: 5,
              iconPath: '/images/5.png',
              position: {
                width: 30,
                height: 40,
                left: windowWidth/2-15,
                top: windowHeight -280

              },
              //是否可点击
              clickable: true
            },
            //添加车辆信息按钮
            {
              id: 6,
              iconPath: '/images/6.png',
              position: {
                width: 30,
                height: 30,
              },
              //是否可点击
              clickable: true
            }
          ]
        })


      },
    })

  },

  //空间被点击事件
  controltap:function(e){
    var that=this;
    var cid=e.controlId;

    switch(cid){
      case 1:{
        var status=myUtils.get("status")
        //根据用户的状态，跳转到相应的界面
        //如果未注册，就跳转到手机注册页面
        if(status==0){
          //跳转到手机注册页面
          wx.navigateTo({
            url: '../register/register',
          })
        }else if(status==1){
          //默认从内存中获取，更改为先从磁盘获取，如果没有找到再从内存中获取
          //跳转到手机充值页面
          wx.navigateTo({
            url: '../deposit/deposit',
          })
        } else if (status == 2) {
          //默认从内存中获取，更改为先从磁盘获取，如果没有找到再从内存中获取
          //跳转到手机充值页面
          wx.navigateTo({
            url: '../identify/identify',
          })
        }
        break;
      }
      case 2: {

        break;
      }
      case 3: {

        break;
      }
      case 4: {
        this.mapCtx.moveToLocation()
        break;
      }
      case 5: {

        break;
      }
      //添加车辆
      case 6: {
        //获取当前已有车辆
        //var bikes=that.data.markers;
        // bikes.push({
        //   iconPath:'/images/7.png',
        //   width:30,
        //   height:30,
        //   latitude:that.data.latitude,
        //   longitude:that.data.longitude
        // });

        this.mapCtx.getCenterLocation({
        //获取当前视野的地理位置
            success: function (res) {
              var lat=res.latitude;
              var log=res.longitude;

              // bikes.push({
              //   iconPath:'/images/7.png',
              //   width:30,
              //   height:30,
              //   //直接把当前的位置赋值给单车位置
              //   latitude:lat,
              //   longitude:log
              // });
              // //重新赋值  
              // that.setData({
              //   markers:bikes
              // })

              //将单车的数据发送到后台（springboot）
              wx.request({
                url: 'http://localhost:8080/bike/add',
                data:{
                  latitude:lat,
                  longitude:log,
                  bikeNo:100010,
                  status:0
                },
                method:'POST',
                success:function(res){
                  console.log(res)
                }
              })
            }
        })
       break;
      }
    }

  },
  /**
   * 地图视野发生变化
   */
  regionchange:function(e){
    // var that=this;
    // var etype=e.type;
    // if(etype=="end")
    // {
    //   this.mapCtx.getCenterLocation({
    //     //获取当前视野的地理位置
    //     success: function (res) {
    //       /**
    //        *console.log(res.longitude) ;console.log(res.latitude)
    //        */
    //       that.setData({
      //       //赋值给页面data
    //         latitude:res.latitude,
    //         longitude:res.longitude
    //       })

      //   }
    //   // });
    //  }


  },

  /**
   * 生命周期-加载页面首次渲染完成
   */
  onReady: function (e) {
    // 使用 wx.createMapContext 获取 map 上下文
    this.mapCtx = wx.createMapContext('myMap')
  },
})
